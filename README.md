# dnsbl_ip_reputation_automated_checking

Released under MIT license.

This is free software; see source for copying conditions. There is no warranty; not even for MERCHANTABILIY or FITNESS FOR A PARTICULAR PURPOSE.

WARNING:
The author will not responsible to any loss, service subscription or cost from all the released codes/scripts in this repository. Please subcribe to any public available IP Reputation services for long-term use, without using the released codes/scripts in this repository. The purpose of released codes/scripts is only for testing or experimental use, please refer to the details of MIT license.
<br />
<br />
<br />
<br />
<br />
Description:

> Scheduled service (systemd timer like CRON), to reverse lookup on specfied IP addresses, and list out those IP addresses, that had been blacklisted by public DNSBL servers or marked as 'BAD' IP reputation. Viewable from browser, where output is dropped in '/var/www/html' (Apache2 root directory).
<br />
PostScript:

> Scripting doesn't make you SLIM, because you are even lazy to interact with browser (yeah! let's automate the interaction with browser with the cool Python Selenium!). Besides, the text that is readable & downloadable by browser, is unable to be blocked by using JavaScript or other methods, if one knows how to be a nerd :)
<br />
ONLY tested on:

> Debian 9 Stable (systemd)
<br />
Prerequisite (depends on):

> xserver-xorg (no desktop environment will also works), apache2 service, dnsutils, python, python-pip, python libraries (please refer in "ip-reputation-server" script in "usr_local_bin" directory)
<br />
YOU CAN:

> *Modify the code/script to run on your own without httpd/www/apache2 service.

> *Run without systemd services for no scheduled plan.

> *Modify the code/script by using PhantomJS / headless chromium / headless firefox / Gecko-engine to run without "xserver-xorg".

> *Modify the code/script to click on "next" button, with less requests to the websites, for better performance.

> *Modify without checking on subnets, but specified IP addresses only.
<br />
Controllable values on DNSBL server and IP address:

> IP address : dnsbl-ip-reputation-checklist

> dnsbl server : dnsbl-server
<br />
<br />
<br />
<br />
<br />
# usr_local_bin: 

Please place all the scripts under "/usr/local/bin" directory, since it is a good practice to have "clean" system, to only mess up the user-reserved "/usr/local" directory, as the File System Hierachy that adhered in Debian.

ALL

> service-dnsbl-ip-reputation

>>> A script to be run by systemd service, where it will automatically run xserver to launch Firefox browser (for "ip-reputation-run-check" script), then shutdown the xserver service when task ended.

> dnsbl-ip-reputation-checklist

>>> List of target ip addresses (/24) to be checked. Hence, duplicated results will happen. For example, "111.111.111.222 and 111.111.111.333", will produce duplicated results for "111.111.111.x" . Do not use comment (#) syntax in this file. Only valided ip addresses, space, newline are accepted.

DNSBL (Domain Name Service Blacklist)

> dnsbl-run-check

>>> Main script to run the reverse lookup operation, where the result range from xxx.xxx.xxx.1 to xxx.xxx.xxx.254 will only be referred. The inline "sleep" is to prevent spike of CPU, after opening thousand for connection at once. Modifying this script to have any subnet range (by default /24) to be referred, or even control how many connections should be opened within the time range.

> dnsbl-thread-forking

>>> The child script that responsible to allow more than one connection to be opened, as background processes at once, when provoked by "dnsbl-run-check".

> dnsbl-html

>>> Tabulate the obtained results into HTML table format.

> dnsbl-server

>>> List of public dnsbl servers for performing reverse lookup. Do not use comment (#) syntax in this file. Only valided URLs, space, newline are accepted.

IP REPUTATION CHECK

> ip-reputation-server

>>> The script that connects and grab all text from website.

> ip-reputation-run-check

>>> The script that runs text processing from website output, and loop from user-defined target subnet (/24)
<br />
<br />
<br />
<br />
<br />
# lib_systemd_system:

Please place these scripts under "/lib/systemd/system", then create softlink to "/etc/systemd/system". Only the "timer" is needed to be run by issuing "systemctl enable dnsbl-ip-reputation-hour.timer && systemctl start dnsbl-ip-reputation-hour.timer".

SYSTEMD SERVICE FILE

> dnsbl-ip-reputation-hour.timer

>>> CRON job is dead in systemd. The alternative way (systemd-timer) to run scheduled service, where comment, uncomment, or modify the parameters to become your own taste's configuration file.

> dnsbl-ip-reputation.service

>>> Please aware of this service is run by "ROOT" user, you should modify to the user with less privilege
<br />
<br />
<br />
<br />
<br />
# var_www_html:

Please place this file under "/var/www/html" directory, and modify your Apache2 configuration to point to this file, thereafter viewable from browser.

> index.html

>>> This webpage will separate the dnsbl and ip reputation results vertically, when viewing from browser. Both separations are scrollable, if long results happen.
