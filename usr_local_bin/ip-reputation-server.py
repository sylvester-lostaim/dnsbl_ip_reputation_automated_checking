# Modified from
# "https://stackoverflow.com/questions/7947579/getting-all-visible-text-from-a-webpage-using-selenium"

#!/usr/bin/env python
import sys,codecs
from contextlib import closing

import lxml.html as html # pip install 'lxml>=2.3.1'
from lxml.html.clean        import Cleaner
from selenium.webdriver     import Firefox         # pip install selenium==2.53.6
from werkzeug.contrib.cache import FileSystemCache # pip install werkzeug

cache = FileSystemCache('.cachedir', threshold=100000)

url="https://talosintelligence.com/reputation_center/lookup?search="+sys.argv[1]+"%2F27"

# get page
page_source = cache.get(url)
if page_source is None:
    # use firefox to get page with javascript generated content
    with closing(Firefox()) as browser:
        browser.get(url)
        page_source = browser.page_source
    cache.set(url, page_source, timeout=60*60) # hour in seconds


# extract text
root = html.document_fromstring(page_source)
# remove flash, images, <script>,<style>, etc
Cleaner(kill_tags=['noscript'], style=True)(root) # lxml >= 2.3.1
result = root.text_content() # extract text
plain = result.encode('utf8')
print plain
